//
//  ViewController.swift
//  test
//
//  Created by Tailor, Chirag on 05/07/2018.
//  Copyright © 2018 Tailor, Chirag. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var switchControl: UISegmentedControl!
    @IBOutlet weak var nextButtonHeightConstriaint: NSLayoutConstraint!
    @IBOutlet weak var spaceBetweenTextViewAndSwitch: NSLayoutConstraint!
    @IBOutlet weak var spaceBetweenButtonAndView: NSLayoutConstraint!
    @IBOutlet weak var textView: UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func switchPressed(_ sender: Any) {
        switch switchControl.selectedSegmentIndex
        {
            case 0:
            //Do all your code for when user has clicked yes. (Currently its default to yes therefore it will show all the fields and button)
                textView.isHidden = false
                spaceBetweenTextViewAndSwitch.constant = 20
                spaceBetweenButtonAndView.constant = 20
                nextButtonHeightConstriaint.constant = 549
                break;
            case 1:
                //If the user clicks no, hide all the text field and move the button up
                textView.isHidden = true
                spaceBetweenTextViewAndSwitch.constant = 0
                spaceBetweenButtonAndView.constant = 0
                nextButtonHeightConstriaint.constant = 20
                break;
            default:
                break;
        }
        
    }
}

